CREATE DATABASE casa_automatizada;
use casa_automatizada;

create table usuario(
    usu_id int primary key auto_increment,
    usu_nome varchar(50),
    usu_email varchar(150),
    usu_cpf varchar(20),
    usu_conta varchar(15),
    usu_banco varchar(30),
    usu_agencia varchar(5),
    usu_tipo varchar(10),
    usu_login varchar(25),
    usu_senha varchar(32),
    usu_confirmado varchar(1) default 'n',
    usu_ativado varchar(1) default 's',
    bit_deletado int default 0
);

create table vigencias(
    vig_id int primary key auto_increment,
    vig_valor double,
    vig_data_gerada date,
    vig_data_exp date,
    vig_data_pago date,
    vig_ativa varchar(1) default 'n',
    vig_usuario_id int,
    bit_deletado int default 0,
    foreign key(vig_usuario_id) references usuario(usu_id)
);

create table dispositivo(
    dis_id int primary key auto_increment,
    dis_nome varchar(25),
    dis_key varchar(32),
    dis_ativo varchar(1) default 's',
    bit_deletado int default 0
);

create table dispositivo_canais(
    disc_id int primary key auto_increment,
    disc_nome varchar(10),
    disc_status int default 0,
    disc_dispositivo_id int,
    disc_ativo varchar(1) default 's',
    bit_deletado int default 0,
    foreign key (disc_dispositivo_id) references dispositivo (dis_id)
);

create table logs(
    log_id int primary key auto_increment,
    log_content longtext,
    log_dispositivo_id int,
    foreign key (log_dispositivo_id) references dispositivo(dis_id)
);

create table aux_usuario_dispositivo(
    aux_id int primary key auto_increment,
    aux_dispositivo_id int,
    aux_usuario_id int,
    aux_ativo varchar(1) default 's',
    bit_deletado int default 0,
    foreign key (aux_dispositivo_id) references dispositivo(dis_id),
    foreign key (aux_usuario_id) references usuario(usu_id)
);

insert into usuario(usu_nome, usu_email, usu_login, usu_senha, usu_confirmado) values ('Administrador', 'admin@admin.com.br', 'admin', '123', 's');
