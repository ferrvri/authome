var mysql = require('mysql');
var fs = require('fs');

var extras = {
    connection: mysql.createConnection(
        {
            host: 'localhost',
            user: 'root',
            password: 'vertrigo',
            database: 'casa_automatizada'
        }
    ),

    writeAFile: (path, content) => {
        fs.writeFileSync(path, content);
        if (fs.existsSync(path)){
            return true;
        }else{
            return false;
        }
    },

    readFile: (path) => {
        if (fs.existsSync(path)){
            return fs.readFileSync(path).toString()
        }else{
            return '';
        }
    }
}

module.exports = extras;

