var fs = require('fs');

var logger = {
    log: (content) => {
        if (fs.existsSync('./logs/log-'+ new Date(Date.now()).toISOString().split('T')[0])){
            var result = fs.readFileSync('./logs/log-'+ new Date(Date.now()).toISOString().split('T')[0]);
            fs.writeFileSync('./logs/log-'+ new Date(Date.now()).toISOString().split('T')[0],
            result+'\r\n'+content);
        }else{
            fs.writeFileSync('./logs/log-'+ new Date(Date.now()).toISOString().split('T')[0],
            content+'\r\n');
        }     
    }
}

module.exports = logger;