var mysql = require('mysql');
var fs = require('fs');

var extras = {
    connection: mysql.createConnection(
        {
            host: 'mysql.technoxinformatica.com.br',
            user: 'technoxinforma01',
            password: 'asdf2019',
            database: 'technoxinforma01'
        }
    ),

    writeAFile: (path, content) => {
        fs.writeFileSync(path, content);
        if (fs.existsSync(path)){
            return true;
        }else{
            return false;
        }
	},

    readFile: (path) => {
        if (fs.existsSync(path)){
            return fs.readFileSync(path).toString()
        }else{
            return '';
        }
    },

    verifyKey: (cb) => {
        if (!fs.existsSync('./rasp.key')) {
            const k = 
                'rasp-authome-' + Math.floor(Math.random() * 15000);
                
            extras.connection.query(
                'insert into dispositivo (dis_nome, dis_key) values(?, md5(?))',
                ['authome-device', k],
                (err, result, fields) => {
                    if (err){
                        console.log('[ERR 101]');
                        throw new {
                            err: 101,
                            desc: 'Device not registered'
                        }
                    }else{
			extras.connection.query('select dis_key from dispositivo where dis_id = ?', [result.insertId], (err, result2, fields) => {
				if (result2.length > 0){
					fs.writeFileSync('./rasp.key', result2[0].dis_key);
		                        console.log('[INFO] New device registered, '+new Date(
		                            new Date().getFullYear(), new Date().getMonth(), new Date().getDate()
		                        ).toLocaleDateString() +'\r\n Key -> '+k);
		
		                        var rr = 0;
		                        extras.connection.query('select * from dispositivo where dis_id = ? and dis_ativo = "s" and bit_deletado = 0', [result.insertId],
		                        (err, result, fields) => {
		                            if (err){
		                                console.log('[ERR 1011]');
		                                throw new {
		                                    err: 1011,
		                                    desc: 'Cannot retrive device info'
		                                }
		                            }else{
		                                console.log('Device info: \r\n');
		                                console.log(JSON.stringify(result));
		                                cb([true, result[0].dis_id, result[0].dis_key])
		                            }
                        		});
				}
			});
                        
                    }
                }        
            );
        } else {
            const r = fs.readFileSync('./rasp.key');
            extras.connection.query('select dis_id, dis_key from dispositivo where dis_key = ? and bit_deletado = 0 and dis_ativo = "s" ', [r.toString().trim()], (err, result, fields) => {
		cb([true, result[0].dis_id, result[0].dis_key])
            });
        }
    }
}

module.exports = extras;

