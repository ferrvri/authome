var io = require('socket.io-client');
var extras = require('./extras');
var gpio = require('onoff').Gpio;
var lcdr = require('lcdi2c');

const lcd = new lcdr(1, 0x3f, 16, 2);
const socket = io.connect('http://technoxinformatica.com.br:21164/');
var myId = 0;
var state = 'unauthorized';

lcd.clear();
lcd.println('TECHNOX', 1);
lcd.println('Iniciando...', 2);
lcd.createChar(1, [0x0,0x1,0x3,0x16,0x1c,0x8,0x0]);

setTimeout( () => {
	lcd.clear();
}, 2000);

setTimeout( () => {
if (extras.connection) {
    console.log('[MYSQL]Connected!');
    lcd.println('[SQL]Conectado', 1);
    if (socket) {
        extras.verifyKey((data) => {
            if (data[0] == true){
                myId = data[1];
                socket.emit('join', {
                    id: data[1],
                    key: data[2]
                });

                console.log('[SOCKET]Trying to connect');
                lcd.println('[SOCKET]Conectando...', 2);
                
		socket.on('loggedin', (data) => {
                    if (data.status == 'authorized'){
                        state = data.status;
                        console.log('[SOCKET] Device authorized, connected!');
			setTimeout( () => {
				lcd.clear();
				lcd.println('[SOCKET]Autorizado', 1);
			}, 1000);
                    }
                });
            }
        });

        socket.on('action', (data) => {
		console.log(data);
		lcd.clear();
		lcd.println('[ACAO] ' +data.channel.disc_gpio + ' - ' + data.type == 'off' ? 'on':'off');
		var gp = new gpio(parseInt(data.channel.disc_gpio), 'out');
		gp.writeSync(data.type == 'off' ? 0 : 1);
        });

	socket.on('disconnect', () => {
		console.log('Disconnected');
		lcd.clear();
		lcd.println('[SOCKET]Desconectado', 1);
	});
    } else {
        console.log('[SOCKET] Not connected!');
	lcd.clear();
	lcd.println('[SOCKET] Nao conectado', 1);
    }
} else {
    console.log('[MYSQL] Not connected!');
    lcd.clear();
    lcd.println('[SQL] Nao conectado', 1);
}

}, 5000);