import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Http } from '@angular/http';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {

  usuario: string = '';
  senha: string = '';

  constructor( private _router: Router, private _http: Http ) { }

  ngOnInit() {
    if (localStorage.getItem('session')){
      this._router.navigate(['/tabs']);
    }
  }

  login(user, pass){
    let h = this._http.post(
      'http://localhost:3000/login',
      {
        login: user,
        senha: pass
      }
    ).subscribe( (response) => {
      if (response.json().status == true){
        localStorage.setItem('session', JSON.stringify(response.json().result[0]));
        this._router.navigate(['/tabs/tab1']);
        h.unsubscribe();
      }
    });
  }


}
