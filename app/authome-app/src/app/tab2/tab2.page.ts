import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { DispositivosCanaisPage } from '../dispositivos-canais/dispositivos-canais.page';

@Component({
  selector: 'app-tab2',
  templateUrl: 'tab2.page.html',
  styleUrls: ['tab2.page.scss']
})
export class Tab2Page implements OnInit{

  dispositivos = [];

  constructor(private _modal: ModalController ) { }

  ngOnInit(){
    this.dispositivos = JSON.parse(localStorage.getItem('session')).dispositivos
    console.log(this.dispositivos);
  }

  async openModal(comp, data)
  {
    const modalPage = await this._modal.create({
      component: comp, 
      componentProps: data
    });

    return await modalPage.present();
  }

  openDisp(disp){
    this.openModal(DispositivosCanaisPage, {dispositivo: disp});
  }
}
