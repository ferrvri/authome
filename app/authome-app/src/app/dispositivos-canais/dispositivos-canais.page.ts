import { Component, OnInit, Input } from '@angular/core';
import { ViewController } from '@ionic/core';
import { ModalController } from '@ionic/angular';
import { Http } from '@angular/http';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-dispositivos-canais',
  templateUrl: './dispositivos-canais.page.html',
  styleUrls: ['./dispositivos-canais.page.scss'],
})
export class DispositivosCanaisPage implements OnInit {

  @Input('dispositivo') dispositivo;

  canais = [];

  constructor( private _modal: ModalController, private _http: Http ) { }

  ngOnInit() {
    this._http.post(
      'http://localhost:3000/devices/channels/'+this.dispositivo.dis_id,
      {}
    ).subscribe( (response) => {
      if (response.json().status == true){
        this.canais = response.json().result;
      }
    });
  }

  sendAction(cn){
    this._http.post(
      'http://localhost:3000/action/device/'+this.dispositivo.dis_id+'/'+cn.disc_id+'/'+(cn.disc_status == 0 ? 1:0),
      {}
    ).subscribe( (response) => {
      if (response.json().status == true){
        
      }else if (response.json().status == 'device-not-socket-connected'){
        Swal({
          title: 'Dispositivo desconectado',
          text: 'Dispositivo não conectado ao servidor principal',
          type: 'warning',
        });
      }
    });
  }
}
