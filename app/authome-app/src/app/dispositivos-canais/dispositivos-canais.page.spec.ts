import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DispositivosCanaisPage } from './dispositivos-canais.page';

describe('DispositivosCanaisPage', () => {
  let component: DispositivosCanaisPage;
  let fixture: ComponentFixture<DispositivosCanaisPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DispositivosCanaisPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DispositivosCanaisPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
